DROP DATABASE GymDatabase;
CREATE DATABASE GymDatabase;
USE GymDatabase;

CREATE TABLE Client(
	clientId CHAR(6),
	fullName VARCHAR(30) NOT NULL,
	email VARCHAR(320),
	DOB DATE,
	gender ENUM('M', 'F'),
	PRIMARY KEY (clientId)
);

CREATE TABLE PT(
	ptId CHAR(6),
	fullName VARCHAR(30) NOT NULL,
	email VARCHAR(320) NOT NULL,
	DOB DATE,
	gender ENUM('M', 'F') NOT NULL,
	PRIMARY KEY (ptId)
);

CREATE TABLE Booking(
	bookingId CHAR(6),
	clientId CHAR(6) NOT NULL,
	ptId CHAR(6) NOT NULL,
	date DATE NOT NULL,
	time TIME NOT NULL,
	duration TIME NOT NULL,
	focus VARCHAR(20),
	PRIMARY KEY (bookingId),
	FOREIGN KEY (clientId)
	REFERENCES Client(clientId),
	FOREIGN KEY (ptId)
	REFERENCES PT(ptId)
);

INSERT INTO Client (clientId, fullName, email, gender)
VALUES ('C00001', 'Olaf', 'Olgood@gmail.com', 'M');

INSERT INTO Client (clientId, fullName, email, DOB, gender)
VALUES ('C00002', 'Artorius', 'Sif@gmail.com', '1992-12-12', 'M');

INSERT INTO Client (clientId, fullName)
VALUES ('C00003', 'Gwyn Sunny');

INSERT INTO PT VALUES ('T00002', 'Lewlight', 'Lewlight+2rules@gmail.com', '1999-10-20', 'M');

INSERT INTO PT VALUES ('T00001', 'Canute', 'Canute_kami@gmail.com','2000-10-03', 'F');

INSERT INTO Booking VALUES ('B00001', 'C00001', 'T00001', '2020-04-15', '19:00:00','02:00:00', 'Weight Loss');

INSERT INTO Booking VALUES ('B00002', 'C00001', 'T00001', '2020-04-17', '17:00:00','01:30:00', 'Weight Loss');

INSERT INTO Booking VALUES ('B00003', 'C00003', 'T00001', '2020-04-16', '18:00:00','02:00:00', 'flexibility');

INSERT INTO Booking VALUES ('B00004', 'C00001', 'T00002', '2020-03-05', '16:00:00','01:00:00', 'Muscle gain');

INSERT INTO Booking VALUES ('B00005', 'C00002', 'T00002', '2020-04-07', '10:00:00','02:00:00', 'Weight Loss');

INSERT INTO Booking VALUES ('B00006', 'C00001', 'T00002', '2020-05-21', '19:00:00','02:00:00', 'Muscle gain');

INSERT INTO Booking VALUES ('B00007', 'C00001', 'T00001', '2020-05-15', '17:00:00','01:30:00', 'Weight Loss');

INSERT INTO Booking VALUES ('B00008', 'C00002', 'T00002', '2020-03-18', '11:00:00','01:00:00', 'flexibility');
