import java.sql.*;
import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.ArrayList;

public class ServeClient implements Runnable{
    //This is the socket that will be used for commuication with the client.
    private Socket sock;

    public ServeClient(Socket sock){
	this.sock = sock;
    }
    
    public void run(){
	String dbUrl = "jdbc:mysql://localhost/GymDatabase";
	String name = "root";
	String pass = "";
	try{
	    //A new connection is established with the database.
	    
	    Connection conn = DriverManager.getConnection(dbUrl, name, pass);
	    Scanner input = new Scanner(sock.getInputStream());
	    PrintWriter output = new PrintWriter(sock.getOutputStream(), true);
	    
	    /*This while loop awaits a command from the client and acts appropriately.*/
	    
	    while(input.hasNextLine()){
	        String command = input.nextLine();
		//If the command is exit the loop breaks.
		if(command.equals("EXIT")){
		    break;
		}
		//Here the method that deals with the command is called//
		commandResponder(command, conn, output);
		//After the method responds to the client the server will respond with "DONE" which tells the client tha the server is done responding.
		output.println("DONE");
	    }
	    //If the client sends "EXIT" the program leaves he loop and closes the conection socket.
	    sock.close();
	}
	catch(SQLException ex){
	    System.out.println("SQL err: "+ex.getMessage());
	}
	catch(IOException ex){
	    System.out.println("IO error:"+ex.getMessage());
	}
    }
    //method returns a ResultSet of all the bookings.
    private ResultSet getAllBookings(Connection conn) throws SQLException{
	String allBookingsQuery = "SELECT * FROM Booking;";
	PreparedStatement ps = conn.prepareStatement(allBookingsQuery);
	return ps.executeQuery();
    }
    //method returns a ResultSet of all the clients.
    private ResultSet getAllClients(Connection conn) throws SQLException{
	String allClientsQuery = "SELECT * FROM Client;";
	PreparedStatement ps = conn.prepareStatement(allClientsQuery);
	return ps.executeQuery();
    }
    //method returns a ResultSet of all the trainers.
    private ResultSet getAllPTs(Connection conn) throws SQLException{
	String allPTsQuery = "SELECT * FROM PT;";
	PreparedStatement ps = conn.prepareStatement(allPTsQuery);
	return ps.executeQuery();
    }
    //method returns a ResultSet of all the bookings for a give pt.
    private ResultSet getBookingsByPtId(Connection conn, String ptId) throws SQLException{
	String bookingsForPtId = "SELECT * FROM Booking WHERE ptId =?;";
	PreparedStatement ps = conn.prepareStatement(bookingsForPtId);
	ps.setString(1, ptId);
	return ps.executeQuery();
    }
    //method returns a ResultSet of all the bookings for a given client.
    private ResultSet getBookingsByClientId(Connection conn, String clientId) throws SQLException{
	String bookingsForClientId = "SELECT * FROM Booking WHERE clientId =?;";
	PreparedStatement ps = conn.prepareStatement(bookingsForClientId);
	ps.setString(1, clientId);
	return ps.executeQuery();
    }
    //method returns a ResultSet of all the bookings for a given date.
    private ResultSet getBookingsByDate(Connection conn, String date) throws SQLException{
	String bookingsForDate = "SELECT * FROM Booking WHERE date =?;";
	PreparedStatement ps = conn.prepareStatement(bookingsForDate);
	ps.setString(1, date);
	return ps.executeQuery();
    }
     
    //This function checks if the id given with the specific chracter is of valid format or not.
    //designated character for Client is:'C', for Booking is:'B' and for Pt is:'T'.
     
    private boolean idFormatCheck(String id, char designatedCharacter){
	if(id.charAt(0) != designatedCharacter){
	    return false;
	}
	if(id.length() != 6){
	    return false;
	}
	for(int i = 1; i < 6; i++){
	    if(Character.isDigit(id.charAt(i)) == false){
		return false;
	    }
	}

	return true;
    }
    //This method checks if this new ID respects entity integrity.
    private boolean respectEntityIntegrity(String id, char designatedCharacter, Connection conn) throws SQLException{
	String tableName;
	String idAttribute;
	if(designatedCharacter == 'C'){
	    tableName = "Client";
	    idAttribute ="clientId";
	}
	else if(designatedCharacter == 'B'){
	    tableName = "Booking";
	    idAttribute = "bookingId";
	}
	else if(designatedCharacter == 'T'){
	    tableName = "PT";
	    idAttribute = "ptId";
	}
	else{
	    System.out.println("designatedCharacter is not recognised.");
	    return false;
	}
	 
	if(idFormatCheck(id, designatedCharacter)){
	    String query = "SELECT "+idAttribute+" FROM "+tableName+";";
	    Statement s = conn.createStatement();
	    ResultSet results = s.executeQuery(query);
	    /*The following loop checks all the ids in the specific table and compares them to the id given to check if they exist or not
	      If they do that means entity integrity is respected. */
	    while(results.next()){
		if(results.getString(1).equals(id)){
		    return false;
		}
	    }
	    return true;
	}
	return false;
    }
    //This method checks if the client or trainer id given exists in thei table and if they do referential integrity is preserved.
    private boolean respectReferentialIntegrity(String id, char designatedCharacter, Connection conn) throws SQLException{
	String tableName;
	String idAttribute;
	if(designatedCharacter == 'C'){
	    tableName = "Client";
	    idAttribute ="clientId";
	}
	else if(designatedCharacter == 'T'){
	    tableName = "PT";
	    idAttribute = "ptId";
	}
	else{
	    return false;
	}
	if(idFormatCheck(id, designatedCharacter)){
	    String query = "SELECT "+idAttribute+" FROM "+tableName+";";
	    Statement s = conn.createStatement();
	    ResultSet results = s.executeQuery(query);
	    while(results.next()){
		if(results.getString(1).equals(id)){
		    return true;
		}
	    }
	    return false;
	}
	return false;
    }
    //This method given a time in a stin format returns its equivalent in seconds.
    private int timeToSecs(String time){
	return Integer.parseInt(time.substring(0,2))*3600 + Integer.parseInt(time.substring(3, 5))*60 + Integer.parseInt(time.substring(6,8));
    }
    //This method checks if the time and date given are availabl for a traine or client.
    private boolean isTimeAvailable(String date, String time, String duration, String id, Connection conn, char designatedCharacter) throws SQLException{
	String idAttribute;
	int enteredBeginning = timeToSecs(time);
	int enteredEnd = timeToSecs(duration) + enteredBeginning;
	 
	if(designatedCharacter == 'C'){
	    idAttribute ="clientId";
	}
	else if(designatedCharacter == 'T'){
	    idAttribute = "ptId";
	}
	else{
	    return false;
	}
	if(idFormatCheck(id, designatedCharacter)){
	    String query = "SELECT date, time, duration FROM Booking WHERE "+idAttribute+" = '"+id+"';";
	    System.out.println(query);
	    Statement s = conn.createStatement();
	    ResultSet results = s.executeQuery(query);
	    /*This while loop goes through all the bookings converts the time to seconds when the dates match and then
	      checks if the intervals clash if they don't that means that the time is good otherwise it is not available.*/
	    while(results.next()){
		if(date.equals(results.getString(1))){
		    int beginningOfSession = timeToSecs(results.getString(2));
		    int endingOfSession = beginningOfSession + timeToSecs(results.getString(3));
		    if(beginningOfSession < enteredEnd && endingOfSession > enteredBeginning){
			return false;
		    }
		}
	    }
	    return true;
	}
	System.out.println("the Id format given for the specific character isnt't correct.");
	return false;
    }
    //Checks if the time string is of correct format.
    private boolean isTimeFormatValid(String time){
	if(time.length()!=8)
	    return false;
	if(time.charAt(2)!=':' || time.charAt(5)!=':')
	    return false;
	for(int i =0; i<time.length(); i++){
	    if(i==2 || i==5)
		continue;
	    else if(Character.isDigit(time.charAt(i)) == false)
		return false;
	}
	return true;
    }
    //Checks if the date string is of correct format.
    private boolean isDateFormatValid(String time){
	if(time.length()!=10)
	    return false;
	if(time.charAt(4)!='-' || time.charAt(7)!='-')
	    return false;
	for(int i =0; i<time.length(); i++){
	    if(i==4 || i==7)
		continue;
	    else if(Character.isDigit(time.charAt(i)) == false)
		return false;
	}
	return true;
    }
	    
    //deletes a booking fo a given booking id.
    private String deleteBooking(String bookingId, Connection conn) throws SQLException{
	if(idFormatCheck(bookingId, 'B')){
	    String query = "DELETE FROM Booking WHERE bookingId = '"+bookingId+"';";
	    Statement s = conn.createStatement();
	    int rowsChanged = s.executeUpdate(query);
	    if(rowsChanged == 1){
		return "The booking: "+bookingId+" has been deleted";
	    }
	    else if(rowsChanged == 0){
		return "The booking: "+bookingId+" has not been deleted most likely not found.";
	    }
	}
	return "Deletion of booking: "+bookingId+" did not happen, id is of invalid format";
    }
    /*This method adds a booking after all the information all of the inputs has been validated,
      and responds with appropriate error message if anything is no validated, or confirmation if 
      boking was added.*/
    
    private String addBooking(ArrayList<String> info, Connection conn) throws SQLException{
	if(info.size()<6 || info.size()>7)
	    return "to create a booking you need 6 or 7 details.";
	if(idFormatCheck(info.get(0), 'B') == false)
	    return "The bookingId given is invalid, correct booking id format example: B12345";
	if(idFormatCheck(info.get(1), 'C') == false)
	    return "The clientId given is invalid, correct client id format example: C12345";
	if(idFormatCheck(info.get(2), 'T') == false)
	    return "The ptId given is invalid, correct trainer id format example: T12345";
	if(isDateFormatValid(info.get(3)) == false)
	    return "The date given is invalid, correct date format example: 1983-11-13";
	if(isTimeFormatValid(info.get(4))==false || isTimeFormatValid(info.get(5))==false)
	    return "The time or duration gicen are invalid. correct time format example: 21:55:00";
	if(info.size() == 7 && info.get(6).length()>20)
	    return "The focus shouldn't have more than 20 characters.";
	if(respectEntityIntegrity(info.get(0), 'B', conn) ==false)
	    return "The booking id given already exists can you give another one.";
	if(respectReferentialIntegrity(info.get(1), 'C', conn) ==false)
	    return "The client Id given doesn't exist in records you should make a new client entry, or use diffrent clientId";
	if(respectReferentialIntegrity(info.get(2), 'T', conn) == false)
	    return "The trainer Id given doesn't exist in records you should make a new trainer entry, or use diffrent pttId";
	if(isTimeAvailable(info.get(3), info.get(4), info.get(5), info.get(1), conn,  'C') ==false)
	    return "The time you gave isn't available for the client, he already has a booking that clashes.";
	if(isTimeAvailable(info.get(3), info.get(4), info.get(5), info.get(2), conn,  'T') ==false)
	    return "The time you gave isn't available for the trainer, he already has a booking that clashes.";
	if(info.size() == 6){
	    String query = "INSERT INTO Booking (bookingId, clientId, ptId, date, time, duration) "+
		"VALUES ('"+info.get(0)+"', '"+info.get(1)+"', '"+info.get(2)+"', '"+info.get(3)+"', '"+info.get(4)+"', '"+info.get(5)+"');";
	    System.out.println("update query: "+query);
	    Statement s =  conn.createStatement();
	    int rowsAffected = s.executeUpdate(query);
	    if(rowsAffected == 1)
		return "The booking was added successfully";
	    else
		return "The booking was not added something went wrong.";
	}
	if(info.size() == 7){
	    String query = "INSERT INTO Booking VALUES ('"+info.get(0)+"', '"+info.get(1)+"', '"+info.get(2)+"', '"+info.get(3)+"', '"+info.get(4)+"', '"+info.get(5)+"', '"+info.get(6)+"');";
	    System.out.println("update query: "+query);
	    Statement s =  conn.createStatement();
	    int rowsAffected = s.executeUpdate(query);
	    if(rowsAffected == 1)
		return "The booking was added successfully";
	    else
		return "The booking was not added something went wrong.";
	}
	return "Something went wrong";
	
    }
    //Adds a new Personal trainer .
    private String addPT(ArrayList<String> info, Connection conn) throws SQLException{
	if(info.size() != 5){
	    return "The number of information given is incorrect you need 5 informations.";
	}
	if(idFormatCheck(info.get(0), 'T') == false){
	    return "The ptId given is of invalid format, correct format example: T00023";
	}
	if(info.get(0).length()>20 || info.get(0).length()==0){
	    return "The name can not be larger than 20 characters";
	}
	String update = "INSERT INTO PT VALUES ('";
	for(int i =0; i<info.size(); i++){
	    if(i == info.size()-1){
		update = update + info.get(i) + "');";
	    }
	    else{
		update = update+info.get(i)+"' ,'";
	    }
	}
	Statement s = conn.createStatement();
	int rowsAffected = s.executeUpdate(update);
	if(rowsAffected ==1)
	    return "The trainer was added successfully";
	else
	    return "The trainer was not added something went wrong.";
    }
    //Adds a new client.
    private String addClient(ArrayList<String> info, Connection conn) throws SQLException{
	if(info.size() != 5){
	    return "The number of information given is incorrect you need 5 informations.";
	}
	if(idFormatCheck(info.get(0), 'C') == false){
	    return "The clientId given is of invalid format, correct format example: C00023";
	}
	if(info.get(0).length()>20 || info.get(0).length()==0){
	    return "The name can not be larger than 20 characters";
	}
	String update = "INSERT INTO Client VALUES ('";
	for(int i =0; i<info.size(); i++){
	    if(i == info.size()-1){
		update = update + info.get(i) + "');";
	    }
	    else{
		update = update+info.get(i)+"' ,'";
	    }
	}
	Statement s = conn.createStatement();
	int rowsAffected = s.executeUpdate(update);
	if(rowsAffected ==1)
	    return "The client was added successfully";
	else
	    return "The client was not added something went wrong.";
    }
    /*This method is the one that uses all of the other methods, it checks the first word in the command sent by the client,
      calls the correct method and responds to the client with appropriate message describing what was done in the database.*/
    
    private void commandResponder(String command, Connection conn, PrintWriter output){
	//This aay list is simply the diffrent words in the command.
	ArrayList<String> elementsOfCommand = stringToElements(command);
	try{
	    if(command.equals("LISTALL")){
		ResultSet results = getAllBookings(conn);
		while(results.next()){
		    output.println("bookingId: "+results.getString(1)+" clientId: "+results.getString(2)+" ptId: "+results.getString(3)+" date: "+results.getString(4)+" time: "+results.getString(5)+
				   " duration: "+results.getString(6)+" focus: "+results.getString(7));
		}
	    }
	    else if(command.equals("SHOWCLIENTS")){
	        ResultSet results = getAllClients(conn);
		while(results.next()){
		    output.println("clientId: "+results.getString(1)+" fullName: "+ results.getString(2)+" email: "+results.getString(3)+" DOB: "+results.getString(4)+" gender: "+results.getString(5));
		}
	    }

	    else if(command.equals("SHOWPTS")){
	        ResultSet results = getAllPTs(conn);
		while(results.next()){
		    output.println("PtId: "+results.getString(1)+" fullName: "+ results.getString(2)+" email: "+results.getString(3)+" DOB: "+results.getString(4)+" gender: "+results.getString(5));
		}
	    }

	    else if(elementsOfCommand.get(0).equals("LISTPT") && elementsOfCommand.size()==2){
		if(idFormatCheck(elementsOfCommand.get(1), 'T')){
		    ResultSet results = getBookingsByPtId(conn, elementsOfCommand.get(1));
		    while(results.next()){
			output.println("bookingId: "+results.getString(1)+" clientId: "+results.getString(2)+" ptId: "+results.getString(3)+" date: "+results.getString(4)+" time: "+results.getString(5)+
				       " duration: "+results.getString(6)+" focus: "+results.getString(7));
		    }
		}
		else{
		    output.println("The ptId given is of invalid format");
		}
	    }
	    else if(elementsOfCommand.get(0).equals("LISTCLIENT") && elementsOfCommand.size()==2){
		if(idFormatCheck(elementsOfCommand.get(1), 'C')){
		    ResultSet results = getBookingsByClientId(conn, elementsOfCommand.get(1));
		    while(results.next()){
			output.println("bookingId: "+results.getString(1)+" clientId: "+results.getString(2)+" ptId: "+results.getString(3)+" date: "+results.getString(4)+" time: "+results.getString(5)+
				       " duration: "+results.getString(6)+" focus: "+results.getString(7));
		    }
		}
		else{
		    output.println("The clientId given is of invalid format");
		}
	    }
	    else if(elementsOfCommand.get(0).equals("LISTDAY") && elementsOfCommand.size()==2){
		ResultSet results = getBookingsByDate(conn, elementsOfCommand.get(1));
		while(results.next()){
		    output.println("bookingId: "+results.getString(1)+" clientId: "+results.getString(2)+" ptId: "+results.getString(3)+" date: "+results.getString(4)+" time: "+results.getString(5)+
				   " duration: "+results.getString(6)+" focus: "+results.getString(7));
		}
	    }
	    else if(elementsOfCommand.get(0).equals("DELETE") && elementsOfCommand.size()==2){
		if(idFormatCheck(elementsOfCommand.get(1), 'B')){
		    String deletionProcess = deleteBooking(elementsOfCommand.get(1), conn);
		    output.println(deletionProcess);
		}
		else{
		    output.println("The booking id given is invalid");
		}
	    }
	    else if(elementsOfCommand.get(0).equals("ADD")){
		ArrayList<String> info = new ArrayList<>();
		for(int i =1; i < elementsOfCommand.size(); i++){
		    info.add(elementsOfCommand.get(i));
		}
		String addingBooking = addBooking(info, conn);
		output.println(addingBooking);
	    }
	    else if(elementsOfCommand.get(0).equals("UPDATE")){
		if(elementsOfCommand.size() != 4){
		    output.println("The UPDATE command takes 3 arguments the bookingId that needs to be updated the attribute that needs to be changed and new value");
		}
		else if(idFormatCheck(elementsOfCommand.get(1), 'B')){
		    String updateQuery = "UPDATE Booking SET "+elementsOfCommand.get(2)+" = ? WHERE bookingId = ?;";
		    PreparedStatement ps = conn.prepareStatement(updateQuery);
		    ps.setString(1, elementsOfCommand.get(3));
		    ps.setString(2, elementsOfCommand.get(1));
		    int rowsAffected = ps.executeUpdate();
		    if(rowsAffected == 1){
			output.println("The update has been done successfully");
		    }
		    else{
			output.println("The update did not happen.");
		    }
		}
		else{
		    output.println("The 1st argument should be a correct format of a the bookingId that needs to be updates.");
		}
	    }
	    else if(elementsOfCommand.get(0).equals("ADDCLIENT")){
		ArrayList<String> info = new ArrayList<>();
		for(int i =1; i < elementsOfCommand.size(); i++){
		    info.add(elementsOfCommand.get(i));
		}
		String addingClient = addClient(info, conn);
		output.println(addingClient);
	    }
	    else if(elementsOfCommand.get(0).equals("ADDPT")){
		ArrayList<String> info = new ArrayList<>();
		for(int i =1; i < elementsOfCommand.size(); i++){
		    info.add(elementsOfCommand.get(i));
		}
		String addingPT = addPT(info, conn);
		output.println(addingPT);
	    }
	    else if(elementsOfCommand.get(0).equals("DELETECLIENT")){
		String query = "DELETE FROM Client WHERE clientId = '"+elementsOfCommand.get(1)+"';";
		Statement s = conn.createStatement();
		int rowsAffected = s.executeUpdate(query);
		if(rowsAffected == 1)
		    output.println("The client "+elementsOfCommand.get(1)+" was deleted.");
		else
		    output.println("The client "+elementsOfCommand.get(1)+" was not deleted something went wrong.");
	    }
	    else if(elementsOfCommand.get(0).equals("DELETEPT")){
		String query = "DELETE FROM Trainer WHERE ptId = '"+elementsOfCommand.get(1)+"';";
		Statement s = conn.createStatement();
		int rowsAffected = s.executeUpdate(query);
		if(rowsAffected == 1)
		    output.println("The trainer "+elementsOfCommand.get(1)+" was deleted.");
		else
		    output.println("The trainer "+elementsOfCommand.get(1)+" was not deleted something went wrong.");
	    }

	    else{
		output.println("Command not recognised");
	    }
	    
		
	}
	catch(SQLException ex){
	    System.out.println(ex.getMessage());
	    output.println(ex.getMessage());
	    output.flush();
	}
    }
    //This method separates a string by spacebar and returns in an arralist the words of that command.
    private ArrayList<String> stringToElements(String command){
	int lastIndex = 0;
	ArrayList<String> result = new ArrayList<>();
	for(int i =0; i < command.length(); i++){
	    if(command.charAt(i) == ' '){
		result.add(command.substring(lastIndex, i));
		lastIndex = i+1;
	    }
	    else if(i == command.length() -1){
		result.add(command.substring(lastIndex, i+1));
	    }
	}
	return result;
    }
}
