import java.net.*;
import java.util.Scanner;
import java.io.*;

public class ClientConn{
    public static void main(String[] args){	
	try{
	    //First a connection to the server is established
	    Socket liaison = new Socket("localhost", Integer.parseInt(args[0]));
	    Scanner in = new Scanner(liaison.getInputStream());
	    Scanner userInput = new Scanner(System.in);
	    PrintWriter out = new PrintWriter(liaison.getOutputStream());
	    //This while loop sends a command in the form of a string to the server and awaits response.
	    while(true){	
		System.out.println("send a command to the server.");
		String input = userInput.nextLine();
		out.println(input);
		out.flush();
		System.out.println("stream flushed");
		//if the user tries to send EXIT the connection will close freeing up the server.
		if(input.equals("EXIT")){
		    liaison.close();
		    System.out.println("Connection closed program will end.");
		    break;
		}
		//This while loop prints out all the lines of response from the server
		//And when the server sends back "DONE" it exits the loop and allows the user to send more commands.
		inner:
		while(in.hasNextLine()){
		    String lineOfResponse = in.nextLine();
		    if(lineOfResponse.equals("DONE")){
			break inner;
		    }
       		    System.out.println(lineOfResponse);
       		}
       		System.out.println("Server responded");
		}
	}
	catch(IOException ex){
	    System.out.println(ex.getMessage());
	}
    }
}
