import javafx.scene.text.Text;
import java.net.*;
import java.io.*;
import java.util.Scanner;

public class ClientThread implements Runnable{
    public String command;
    public Scanner in;
    public PrintWriter out;
    public String result;
    public Text t;
    
    public ClientThread(Socket s, String command, Text text){
	try{
	    t = text;
	    in = new Scanner(s.getInputStream());
	    out = new PrintWriter(s.getOutputStream(), true);
	    this.command = command;
	    
	}
	catch(IOException ex){}
    }
	
    public void run(){
	String result = "";
	out.println(command);
      
	while(in.hasNextLine()){
	    String serverMessage = in.nextLine();
	    if(serverMessage.equals("DONE"))
		break;
	    result = result + "\n"+serverMessage;	    
	}
	t.setText(result);
    }
}
