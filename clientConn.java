import java.net.*;
import java.util.Scanner;
import java.io.*;

public class clientConn{
    public static void main(String[] args){	
	try{
	    Socket liaison = new Socket("localhost", Integer.parseInt(args[0]));
	    Scanner in = new Scanner(liaison.getInputStream());
	    Scanner userInput = new Scanner(System.in);
	    PrintWriter out = new PrintWriter(liaison.getOutputStream());
	    while(true){	
		System.out.println("send a command to the server.");
		String input = userInput.nextLine();
		out.println(input);
		out.flush();
		System.out.println("stream flushed");
		if(input.equals("EXIT")){
		    liaison.close();
		    System.out.println("Connection closed program will end.");
		    break;
		}
		inner:
		while(in.hasNextLine()){
		    String lineOfResponse = in.nextLine();
		    if(lineOfResponse.equals("DONE")){
			break inner;
		    }
       		    System.out.println(lineOfResponse);
       		}
       		System.out.println("Server responded");
		}
	}
	catch(IOException ex){
	    System.out.println(ex.getMessage());
	}
    }
}
