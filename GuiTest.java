
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.text.*;
import java.net.*;
import java.io.*;
import java.util.Scanner;


public class GuiTest extends Application {
    Stage stage;
    Scene firstScene; //This will be the main scene from which you can go to diffrent scenes.
    String command = "";
    Socket s;
    @Override
    public void start(Stage primaryStage) {
	//This creates the scen that takes the port number necessary for the server connection
	//And the main home scene.
        stage = primaryStage;
        primaryStage.setTitle("Personnal Trainers Gym");
        BorderPane pane = new BorderPane();
        HBox middle = new HBox(20);
        middle.setPadding(new Insets(30));
        Button a, b, c;
        c = new Button("Manage Bookings");
        b = new Button("Manage Clients");
        a = new Button("Manage Trainers");
        c.setOnAction(e -> stage.setScene(manageBooking()));
        b.setOnAction(e -> stage.setScene(manageClient()));
        a.setOnAction(e -> stage.setScene(manageTrainer()));
        middle.getChildren().add(c);
        middle.getChildren().add(b);
        middle.getChildren().add(a);
        pane.setCenter(middle);
        firstScene = new Scene(pane, 550, 200);
        primaryStage.setScene(firstScene);
        primaryStage.show();
        Text title = new Text(50, 50, "Personnal trainer GYM.");
        title.setFont(Font.font("Courier", FontWeight.BOLD, 30));
        title.setFill(Color.BLACK);
        pane.setTop(title);
        HBox bottomButton = new HBox();
        pane.setBottom(bottomButton);
	VBox box = new VBox(20);
	box.setPadding(new Insets(30));
	box.getChildren().add(new Label("give the port number"));
	TextField input = new TextField();
	box.getChildren().add(input);
	Button d = new Button("start connection");
	d.setOnAction(e -> {
		try{
		    s = new Socket("localhost", Integer.parseInt(input.getText()));
		}
		catch(IOException ex){
		    System.out.println("IO error: "+ex.getMessage());
		    System.exit(0);
		}
		stage.setScene(firstScene);
	    });
	box.getChildren().add(d);
	Scene connectionScene = new Scene(box);
	stage.setScene(connectionScene);
	
    }
    
    public static void main(String[] args){
	
        
        Application.launch(args);
        
    }

    //Creates the scene for managing the booking.
    public Scene manageBooking(){
        BorderPane pane = new BorderPane();        
        VBox box = new VBox(20);
        pane.setCenter(box);
        box.setPadding(new Insets(30));
        Button a, b, c, d, e, f, g, h;
	//Each button changes the scene to make it work.
        a = new Button("Show  bookings");
        a.setOnAction(k -> stage.setScene(simpleDisplay("Booking")));
        b = new Button("Add booking");
        b.setOnAction(k -> stage.setScene(addBooking()));
        c = new Button("List bookings by client");
        c.setOnAction(k -> stage.setScene(listBookingBy("Client")));
        d = new Button("List bookings by trainer");
        d.setOnAction(k -> stage.setScene(listBookingBy("Trainer")));
        e = new Button("List bookings by date");
        e.setOnAction(k -> stage.setScene(listBookingBy("Date")));
        f = new Button("Delete booking");
        f.setOnAction(k -> stage.setScene(deleteRow("Booking")));
        g = new Button("Update booking");
        g.setOnAction(k -> stage.setScene(updateBooking()));
        h = new Button("Go home");
        h.setOnAction(k -> stage.setScene(firstScene));
        box.getChildren().add(a);
        box.getChildren().add(b);
        box.getChildren().add(c);
        box.getChildren().add(d);
        box.getChildren().add(e);
        box.getChildren().add(f);
        box.getChildren().add(g);
        box.getChildren().add(h);
        Text title = new Text(50, 50, "Personnal trainer GYM");
        title.setFont(Font.font("Courier", FontWeight.BOLD, 30));
        title.setFill(Color.BLACK);
        pane.setTop(title);
        Scene scene = new Scene(pane);
        return scene;
    }
    //This creates the managing clients
    public Scene manageClient(){
        BorderPane pane = new BorderPane();        
        VBox box = new VBox(20);
        pane.setCenter(box);
        box.setPadding(new Insets(30));
        Button a, b, c, d;
        a = new Button("Show  Clients");
        a.setOnAction(e -> stage.setScene(simpleDisplay("Client")));
        b = new Button("Add Client");
        b.setOnAction(e -> stage.setScene(addPerson("Client")));
        c = new Button("Delete Client");
        c.setOnAction(e -> stage.setScene(deleteRow("Client")));
        d = new Button("Go home");
        d.setOnAction(k -> stage.setScene(firstScene));
        box.getChildren().add(a);
        box.getChildren().add(b);
        box.getChildren().add(c);
        box.getChildren().add(d);
        Text title = new Text(50, 50, "Personnal trainer GYM");
        title.setFont(Font.font("Courier", FontWeight.BOLD, 30));
        title.setFill(Color.BLACK);
        pane.setTop(title);
        Scene scene = new Scene(pane);
        return scene;
    }
    //This is the scene for managing the trainers
    public Scene manageTrainer(){
        BorderPane pane = new BorderPane();        
        VBox box = new VBox(20);
        pane.setCenter(box);
        box.setPadding(new Insets(30));
        Button a, b, c, d;
        a = new Button("Show  trainer");
        a.setOnAction(e -> stage.setScene(simpleDisplay("Trainer")));
        b = new Button("Add trainer");
        b.setOnAction(e -> stage.setScene(addPerson("Trainer")));
        c = new Button("Delete trainer");
        c.setOnAction(e -> stage.setScene(deleteRow("Trainer")));
        d = new Button("Go home");
        d.setOnAction(k -> stage.setScene(firstScene));
        box.getChildren().add(a);
        box.getChildren().add(b);
        box.getChildren().add(c);
        box.getChildren().add(d);
        
        Text title = new Text(50, 50, "Personnal trainer GYM");
        title.setFont(Font.font("Courier", FontWeight.BOLD, 30));
        title.setFill(Color.BLACK);
        pane.setTop(title);
        Scene scene = new Scene(pane, 600, 600);
        return scene;
    }
    
    public Scene addBooking(){
        GridPane pane = new GridPane();
        String[] fieldNames = {"Booking Id", "Client Id", "Trainer Id", "Date", 
        "Time", "Duration", "focus"};
        TextField[] inputs = {new TextField(), new TextField(), new TextField(), new TextField(), 
            new TextField(), new TextField(), new TextField()};
        for(int i =0; i<7; i++){
            pane.add(new Label(fieldNames[i]), 0, i+1);
            pane.add(inputs[i], 1, i+1);
        }
	Text serverResponse = new Text("Server Response");
        Button a, b;
        a = new Button("Add the booking");
	//For the buttons that are supposed to get results from the server they start a thread that does the communication
	//with the server and that thread changes the Text object displaying the result. This logic is used for all the
	//The buttons that get results. The lambda expression from the button also creates the command that will be sent to
	//The server from the input fields.
	a.setOnAction(e -> {
		command = "ADD";
		for(int i =0; i< 7; i++)
		    command = command+" "+inputs[i].getText();
		Thread t = new Thread(new ClientThread(s, command, serverResponse));
		t.start();
	    });
        b = new Button("Go Home");
        b.setOnAction(e -> stage.setScene(firstScene));
        pane.add(serverResponse, 0, 8);
        pane.add(a, 1, 8);
        pane.add(b, 0, 9);
        pane.setHgap(20);
        pane.setVgap(10);
        Scene scene = new Scene(pane, 1000, 600);
        return scene;
    }
    public Scene addPerson(String s){
	String commandName = "";
	if(s.equals("Client"))
	    commandName = "ADDCLIENT";
	else
	    commandName = "ADDTRAINER";
        GridPane pane = new GridPane();
        String[] fieldNames = {" Id", " Name", " email", " DOB", " Gender"};
        TextField[] inputs = {new TextField(), new TextField(), new TextField(), new TextField(), new TextField()};
        for(int i = 0; i<5; i++){
            pane.add(new Label(s + fieldNames[i]), 0, i+1);
            pane.add(inputs[i], 1, i+1);
        }
	Text serverResponse =  new Text("Status message");
        Button a, b;
        a = new Button("Add the " + s);
	command = commandName;
	a.setOnAction(e -> {
		
		for(int i =0; i< 5; i++)
		    command = command+" "+inputs[i].getText();
		Thread t = new Thread(new ClientThread(this.s, command, serverResponse));
		t.start();
	    });
        b = new Button("Go Home");
        b.setOnAction(e -> stage.setScene(firstScene));
	pane.add(serverResponse, 0, 8);
        
        pane.add(a, 1, 8);
        pane.add(b, 0, 9);
        pane.setHgap(20);
        pane.setVgap(10);
        Scene scene = new Scene(pane, 1000, 600);
        return scene;
    }
    
    public Scene listBookingBy(String s){
	String commandName = "";
	if(s.equals("Client"))
	    commandName = "LISTCLIENT";
	else if(s.equals("Trainer"))
	    commandName = "LISTPT";
	else if(s.equals("Date"))
	    commandName = "LISTDATE";
        VBox box = new VBox(20);
	Text serverResponse = new Text("Server response");
        box.getChildren().add(serverResponse);
        TextField input = new TextField();
        box.getChildren().add(input);
        Button a, b;
        a = new Button("Get Bookings");
	command = commandName+" "+input.getText();
	a.setOnAction(e -> {
		
		Thread t = new Thread(new ClientThread(this.s, command, serverResponse));
		t.start();
	    });
        b = new Button("Go Home.");
        b.setOnAction(e -> stage.setScene(firstScene));
        a.setOnAction(null);
        box.getChildren().add(a);
        box.getChildren().add(new Label("Result space"));
        box.getChildren().add(b);
        box.setPadding(new Insets(30));
        Scene scene = new Scene(box, 1000, 600);
        return scene;
    }
    
    public Scene deleteRow(String s){
	String commandName ="";
	if(s.equals("Booking"))
	    commandName = "DELETE";
	else if(s.equals("Client"))
	    commandName = "DELETECLIENT";
	else if(s.equals("Trainer"))
	    commandName = "DELETEPT";
        VBox box = new VBox(20);
	Text serverResponse = new Text("Server response");
	TextField input = new TextField();
        Button a, b;
        a = new Button("Delete "+s);
	command = commandName;
	a.setOnAction(e -> {
		command = command+" "+input.getText();
		
		Thread t = new Thread(new ClientThread(this.s, command, serverResponse));
		t.start();
	    });
        b = new Button("Go Home.");
        b.setOnAction(e -> stage.setScene(firstScene));
        box.getChildren().add(new Label("Enter " + s + " Id"));
        
        box.getChildren().add(input);
        box.getChildren().add(a);        
        box.getChildren().add(serverResponse);
        box.getChildren().add(b);
        box.setPadding(new Insets(30));
        Scene scene = new Scene(box, 1000, 600);
        return scene;
    }
    
    public Scene updateBooking(){
        GridPane pane = new GridPane();
        String[] fieldNames = {"Enter BookingId", "Enter attribute name", "Enter new value"};
        TextField[] inputs = {new TextField(), new TextField(), new TextField()};
        for(int i = 0; i<3; i++){
            pane.add(new Label(fieldNames[i]), 0, i+1);
            pane.add(inputs[i], 1, i+1);
        }
        Text serverResponse = new Text("Server status");
        Button a = new Button("Update Booking");
	a.setOnAction(e -> {
		command = "UPDATE";
		for(int i=0; i<3; i++)
		    command = command+" "+inputs[i];
		
		Thread t = new Thread(new ClientThread(s, command, serverResponse));
		t.start();
	    });
        Button b = new Button("Go Home.");
        pane.add(serverResponse, 0, 4);
        pane.add(a, 1, 4);
        pane.add(b, 0, 5);
        pane.setHgap(20);
        pane.setVgap(10);
        return new Scene(pane, 1000, 600);
    }
    
    public Scene simpleDisplay(String s){
	String commandName = "";
	if(s.equals("Booking"))
	    commandName = "LISTALL";
	else if(s.equals("Client"))
	    commandName = "SHOWCLIENTS";
	else if(s.equals("Trainer"))
	    commandName = "SHOWPTS";
	
        VBox box =  new VBox(20);
        Label declaration = new Label("List of all "+s);
        Text serverResponse = new Text("Server Response");
	Button b = new Button("Get the "+s);
	b.setOnAction(e -> {
		
		Thread t = new Thread(new ClientThread(this.s, command, serverResponse));
		t.start();
	    });
        Button a = new Button("Go Home");
	command = commandName;
        a.setOnAction(e -> stage.setScene(firstScene));
        box.getChildren().add(declaration);
        box.getChildren().add(serverResponse);
	box.getChildren().add(b);
        box.getChildren().add(a);
        box.setPadding(new Insets(30));
        Scene scene = new Scene(box, 1000, 600);
        return scene;
    }
    
	    
}
