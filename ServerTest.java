import java.net.*;
import java.util.Scanner;
import java.io.*;

public class ServerTest{
    public static void main(String[] args) throws IOException{
	ServerSocket serverConn = new ServerSocket(8889);
	outer:
	while(true){
	    Socket liaison = serverConn.accept();
	    try{
		PrintWriter out = new PrintWriter(liaison.getOutputStream());
		Scanner in = new Scanner(liaison.getInputStream());
		System.out.println("waiting for a message from client.");
		while(in.hasNext()){
		    String input = in.nextLine();
		    System.out.println(input);
		    out.println(input);
		    out.flush();
		    if(input.equals("EXIT")){
			liaison.close();
		        break outer;
		    }
		}
	    }
	    catch(IOException ex){
		System.out.println(ex.getMessage());
	    }
	}
    }
}
	
